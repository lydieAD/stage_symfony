<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('username')
            ->add('password',PasswordType::class)
            ->add('roles',ChoiceType::class,[
                'label'=>'Rôles',
                'choices' => [
                    'Utilisateur'=> 'ROLE_USER',
                    'Editeur'=>'ROLE_EDITOR',
                    'Administrateur'=>'ROLE_ADMIN'
                ],
                'multiple' => true,
                'expanded' => true,
                'required' => true
            ])
            ->add('valide',SubmitType::class, [
                'label' => 'Valider'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
