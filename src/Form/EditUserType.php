<?php

namespace App\Form;

use App\Entity\User;
//use ContainerBqFitGF\getForm_Type_ChoiceService;
use ContainerUEAp1dO\getForm_Type_ChoiceService;
use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpKernel\Exception\NotAcceptableHttpException;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class EditUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class,[
                'constraints'=>[
                    new NotBlank([
                        'message'=>'merci de saisir une adresse email'
                    ])

                ],
                'required'=> true,
                'attr'=>[
                    'class'=>'form-control'
                ]
            ])
//            ->add('email')
            ->add('username')
            ->add('password',PasswordType::class)


            ->add('roles',ChoiceType::class,[
                'label'=>'Rôles',
                'choices' => [
                    'Utilisateur'=> 'ROLE_USER',
                    'Editeur'=>'ROLE_EDITOR',
                    'Administrateur'=>'ROLE_ADMIN'
                ],
                'multiple' => true,
                'expanded' => true,
                'required' => true
            ])
            ->add('valide',SubmitType::class, [
                'label' => 'Valider'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
