<?php

namespace App\Form;

use FOS\CKEditorBundle\Form\Type\CKEditorType;
use App\Entity\Module;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;



class ModuleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',  TextType::class, [
                    'label' => 'Titre du module',
                    'required' => true,
                    'attr' => [
                        'placeholder' => 'Titre'
            ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez renseigner un titre de module'
                    ])
                ]
            ])

            ->add('autor', TextType::class, [
                'label' => 'Auteur du module',
                'required' => true,
                'attr' => [
                    'placeholder' => 'entrez votre Nom'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez renseigner un Nom'
                    ])
                ]
            ])

            ->add('content', CKEditorType::class,[
        'label' => 'contenu du module',
        'required' => true,
        'attr' => [
            'placeholder' => 'entrez votre contenu'
        ],
        'constraints' => [
            new NotBlank([
                'message' => 'Veuillez renseigner un Nom'
            ])
        ]
    ])

            ->add('featured_image', FileType::class, [
                    'label' => 'fichier',
//                    'require'=> 'true',
                    'required' => false])


            ->add('categorie',TextType::class,[
                'label' => 'catégorie',
                'required' => true,
                'attr' => [
                    'placeholder' => 'entrer le catégorie du module'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez renseigner la catégorie'
                    ])
                ]
            ])


//            ->add('categorie',ChoiceType::class,[
//                'label' => 'catégorie',
//                'choices'=>[
//                    'qualité'=>'qualite',
//                    'autre'=>'autre'
//                ],
//                'multiple'=>true,
//                'expanded'=>true,
//                'required' => true,
//            ])
//



            ->add('envoyer', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-dark',
                ]
            ]);

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Module::class,
        ]);
    }
}
