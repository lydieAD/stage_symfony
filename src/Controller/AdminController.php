<?php

namespace App\Controller;



use App\Entity\User;

use App\Form\CreateUserType;
use App\Form\EditUserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Exception;
use Symfony\Component\Routing\Annotation\Route;

/**
 *
 * @Route("/admin", name="admin_")
 */
class AdminController extends AbstractController
{
    /**
     * @Route ("/", name="index")
     */
    public function index()
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    /**
     * liste des utilisateurs du site
     * @Route ("/utilisateur", name="utilisateurs")
     */
    public function usersList(UserRepository $user)
    {
        return $this->render("admin/user.html.twig", [
            'users' => $user->findAll()
        ]);

    }

    /**
     * modifier un utilisateur
     * @Route("/utilisateur/modifier/{id}", name="modifier_utilisateur")
     */
    public function editUser(User $User, Request $request)
    {
        $form = $this->createForm(EditUserType::class, $User);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($User);
            $entityManager->flush();

            $this->addFlash('message', 'Utilisateur modifié avec succès');
            return $this->redirectToRoute('admin_utilisateurs');

        }
        return $this->render('admin/edituser.html.twig', [
            'userForm' => $form->createView()
        ]);

    }




}
