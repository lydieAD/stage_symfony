<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LmsController extends AbstractController
{
    /**
     *
     * @Route ("/", name="home")
     */
    public function index(): Response
    {
        return $this->render('lms/home.html.twig', [
            'controller_name' => 'LmsController',
        ]);
    }


}


